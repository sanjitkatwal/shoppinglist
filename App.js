import React, {useState} from 'react'
import { View, Text, Image, StyleSheet, FlatList } from 'react-native'
import Header from './components/Header'
import ListItem from './components/ListItem'

const App = () => {
  const [items, setItems] = useState([
    {id: 1, text: 'Milk'},
    {id: 2, text: 'Eggs'},
    {id: 3, text: 'Bread'},
    {id: 4, text: 'Juice'},
    {id: 5, text: 'SK'},
    {id: 6, text: 'JOhn'},
  ]);

  const deleteItem = (id) =>{
    setItems(prevItems => {
        return prevItems.filter(item => item.id != id);
    });
  }

  return (
    <View style={style.container}>
      <Header />
      <FlatList
        data={items} 
        renderItem={({item}) => <ListItem item={item} deleteItem={deleteItem} />}
      
      />
      {/* <Image source={{uri: 'https://randomuser.me/api/portraits/men/99.jpg'}} style={style.img} /> */}
    </View>
  );
};



const style = StyleSheet.create({
  container:{
    flex:1,
    paddingTop: 5,
  },
  
  text: {
    color:'darkslateblue', 
    fontSize:30
  }, 

  img: {
    width: 100,
    height: 100,
    borderRadius: 100 / 3,
  }
});


export default App;